package com.gdc.exercise;

import java.io.*;
import java.nio.charset.Charset;

public class MyIOHelper implements IOHelper {

    /**
     * Копирует байты из {@code InputStream} в {@code OutputStream}.
     *
     * @param in  входной поток
     * @param out выходной поток
     * @return число скопированных байт
     */
    public long copy(InputStream in, OutputStream out) throws IOException {
        OutputStreamCounter outStream = new OutputStreamCounter(out);
        int c;
        while ((c = in.read()) != -1) {
            outStream.write(c);
        }
        return outStream.getCount();
    }

    /**
     * Копирует указанный файл.
     *
     * @param source копируемый файл
     * @param target файл в который копировать
     * @return размер скопированных данных в байтах
     */
    public long copy(File source, File target) throws IOException {
        try (InputStream in = new BufferedInputStream(new FileInputStream(source));
             OutputStream out = new BufferedOutputStream(new FileOutputStream(target))) {
            return copy(in, out);
        }
    }

    public String readFile(File file) throws IOException {
        return readFile(file, Charset.defaultCharset().toString());
    }

    /**
     * Считывает файл в строку.
     *
     * @param file     файл файл для считывания
     * @param encoding кодировка
     * @return содержимое файла в виде строки
     */
    public String readFile(File file, String encoding) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            return sb.toString();
        }
    }

    /**
     * Записывает строку в файл.
     *
     * @param file     файл для записи
     * @param content  содержимое
     * @param encoding кодировка, если {@code null}, то будет использована системная кодировка
     * @param append   если {@code true}, то добавить строку к существующему содержимому,
     *                 если {@code false}, то перезаписать содержимое
     */
    public void writeFile(File file, String content, String encoding, boolean append) throws IOException {
        if (append) {
            try (Writer writer = new OutputStreamWriter(new FileOutputStream(file, true), encoding)) {
                writer.append(content);
            }
        } else {
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), encoding))) {
                writer.write(content);
            }
        }
    }

    /**
     * Создает {@code InputStream}, читающий указанную строку.
     *
     * @param source строка для чтения
     * @return {@code InputStream}
     */
    public InputStream createInputStream(String source) throws IOException {
        byte[] target = source.getBytes();
        return createInputStream(target);
    }

    /**
     * Создает {@code InputStream}, читающий указанную строку.
     *
     * @param source   строка для чтения
     * @param encoding кодировка
     * @return {@code InputStream}
     */
    public InputStream createInputStream(String source, String encoding) throws IOException {
        byte[] target = source.getBytes(encoding);
        return createInputStream(target);
    }

    private InputStream createInputStream(byte[] bytes) {
        return new ByteArrayInputStream(bytes);
    }

}
