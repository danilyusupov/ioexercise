package com.gdc.exercise;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;

import static org.junit.Assert.*;

public class MyIOHelperTest {

    private final String tmpDir = System.getProperty("java.io.tmpdir");
    private MyIOHelper helper = new MyIOHelper();
    private String testText = "Привет. This is a test version of base text file. Escape.\n";
    private final String UTF = "UTF-8";
    private final String ASCII = "US-ASCII";
    private final String WIN = "Windows-1251";
    private final File source = getFile("source.txt");
    private final File target = getFile("target.txt");

    @Before
    public void preTest() {
        source.delete();
        target.delete();
    }

    /**
     * OK
     */
    @Test
    public void copy() throws IOException {
        try (InputStream in = new BufferedInputStream(new ByteArrayInputStream(testText.getBytes()));
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            long size = helper.copy(in, out);
            assertEquals(testText.getBytes().length, size);
        }
    }

    /**
     * OK
     */
    @Test
    public void zeroBytesCopy() throws IOException {
        byte[] arr = new byte[0];
        try (InputStream in = new BufferedInputStream(new ByteArrayInputStream(arr));
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            //IOException пробрасывается из метода copy()
            long size = helper.copy(in, out);
            assertEquals(arr.length, out.size());
            assertEquals(arr.length, size);
        }
    }

    /**
     * OK
     */
    @Test
    public void copyFiles() throws IOException {
        source.createNewFile();
        long size = helper.copy(source, target);
        assertEquals(source.length(), target.length());
        assertEquals(target.length(), size);
    }

    /**
     * OK
     */
    @Test
    public void copyEmptyFiles() throws IOException {
        source.createNewFile();
        long size = helper.copy(source, target);
        assertEquals(source.length(), target.length());
        assertEquals(target.length(), size);
    }

    /**
     * OK
     */
    @Test
    public void readFile() throws IOException {
        helper.writeFile(source, testText, Charset.defaultCharset().toString(), false);
        String result = helper.readFile(source);
        assertEquals(testText.length(), result.length());
        assertFalse(result.contains("?"));
    }

    /**
     * OK
     */
    @Test
    public void readEmptyFile() throws IOException {
        helper.writeFile(source, "", Charset.defaultCharset().toString(), false);
        String result = helper.readFile(source);
        assertEquals(0, result.length());
    }

    /**
     * OK
     */
    @Test
    public void readWrongEncodedFile() throws IOException {
        helper.writeFile(source, testText, ASCII, false);
        String result = helper.readFile(source);
        assertTrue(result.contains("?"));
    }

    /**
     * OK
     */
    @Test
    public void readFileWithRightEncoding() throws Exception {
        helper.writeFile(source, testText, WIN, false);
        String result = helper.readFile(source, WIN);
        assertFalse(result.contains("?"));
    }

    /**
     * OK
     */
    @Test
    public void readFileWithWrongEncoding() throws IOException {
        helper.writeFile(source, testText, UTF, false);
        String result = helper.readFile(source, ASCII);
        assertTrue(result.contains("�"));
    }

    /**
     * OK
     */
    @Test
    public void readEmptyFileEncoded() throws IOException {
        helper.writeFile(source, "", ASCII, false);
        String result = helper.readFile(source, ASCII);
        assertEquals(0, result.length());
    }

    /**
     * OK
     */
    @Test
    public void writeNotAppendFile() throws IOException {
        helper.writeFile(source, testText, UTF, false);
        assertEquals(testText, helper.readFile(source, UTF));
    }

    /**
     * OK
     */
    @Test
    public void writeAppendedFile() throws IOException {
        helper.writeFile(source, testText, WIN, false);
        helper.writeFile(source, testText, WIN, true);
        assertEquals(testText + testText, helper.readFile(source, WIN));
    }

    /**
     * OK
     */
    @Test
    public void emptyAppendWritingFile() throws IOException {
        helper.writeFile(source, testText, UTF, false);
        helper.writeFile(source, "", UTF, true);
        assertEquals(testText, helper.readFile(source, UTF));
    }

    /**
     * OK
     */
    @Test
    public void createInputStream() throws IOException {
        try (InputStream in = helper.createInputStream(testText);
             OutputStream out = new ByteArrayOutputStream()) {
            long size = helper.copy(in, out);
            assertEquals(testText.getBytes().length, size);
        }
    }

    /**
     * OK
     */
    @Test
    public void createEmptyInputStream() throws IOException {
        try (InputStream in = helper.createInputStream("");
             OutputStream out = new ByteArrayOutputStream()) {
            long size = helper.copy(in, out);
            assertEquals(0, size);
        }
    }

    /**
     * OK
     */
    @Test
    public void createEncodedInputStream() throws Exception {
        try (InputStream in = helper.createInputStream(testText, UTF);
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            helper.copy(in, out);
            assertEquals(testText, new String(out.toByteArray(), UTF));
        }
    }

    @After
    public void postTest() {
        source.delete();
        target.delete();
    }

    private File getFile(String filename) {
        return new File(tmpDir + filename);
    }
}